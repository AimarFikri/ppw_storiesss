from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'index.html')

def about(request):
    return render(request, 'about.html')

def skill(request):
    return render(request, 'skills.html')

def connect(request):
    return render(request, 'connect.html')

def hobbies(request):
    return render(request, 'hobbies.html')

