from django.urls import path

from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('hobbies/', views.hobbies, name='hobbies'),
    path('about/', views.about, name='about'),
    path('skills/', views.skill, name='skills'),
    path('connect/', views.connect, name='connect')
]
